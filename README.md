# SABNAtk - Fast Counting in Machine Learning Applications

## Abstract
We propose scalable methods to execute counting queries in machine learning applications. To achieve memory and computational efficiency, we abstract counting queries and their context such that the counts can be aggregated as a stream. We demonstrate performance and scalability of the resulting approach on random queries, and through extensive experimentation using Bayesian networks learning and association rule mining. Our methods significantly outperform commonly used ADtrees and hash tables, and are practical alternatives for processing large-scale data.

## Introduction
This repository serves as extension to the experimental results section of the paper titled "Fast Counting in Machine Learning Applications." It contains extensive results (including additional test cases), together with the data, source code, and scripts that can be used to reproduce our experiments.

Experimental details have been clearly described in the paper, so in this repository we avoid redundant information. Focussing only on supplementary information which was left out from the paper due to space constraints.


Following table summarizes the data (downloadable) used for the various experiments.   

| Dataset       |    n    |  Range of r<sub>i</sub> | Average r<sub>i</sub> |           m      |
| :--------     | :-----: | :---------------------: | :-------------------: |  ------------    |
| Child         |   20    |  2-6                    |            3          | [1K][d_1k_child] [10K][d_10k_child] [100K][d_100k_child] [1M][d_1m_child]  [10M][d_10m_child] |
| Insur(ance)   |   27    |  2-5                    |            3.3        | [1K][d_1k_insur] [10K][d_10k_insur] [100K][d_100k_insur] [1M][d_1m_insur]  [10M][d_10m_insur] |
| Mild(ew)      |   35    |  3-99                   |            16.4       |[1K][d_1k_mild] [10K][d_10k_mild] [100K][d_100k_mild] [1M][d_1m_mild]  [10M][d_10m_mild] |
| Alarm         |   37    |  2-4                    |            2.83       | [1K][d_1k_alarm] [10K][d_10k_alarm] [100K][d_100k_alarm] [1M][d_1m_alarm]  [10M][d_10m_alarm] |
| Barley        |   48    |  3-67                   |            9.02       | [1K][d_1k_barley] [10K][d_10k_barley] [100K][d_100k_barley] [1M][d_1m_barley]  [10M][d_10m_barley] |
| Hail(finder)  |   56    |  2-11                   |            3.98       | [1K][d_1k_hail] [10K][d_10k_hail] [100K][d_100k_hail] [1M][d_1m_hail]  [10M][d_10m_hail] |
| Hepar2        |   70    |  2-4                    |            2          |[1K][d_1k_hepar2] [10K][d_10k_hepar2] [100K][d_100k_hepar2] [1M][d_1m_hepar2]  [10M][d_10m_hepar2] |
| Win95(pts)    |   74    |  2-2                    |            2          | [1K][d_1k_win95] [10K][d_10k_win95] [100K][d_100k_win95] [1M][d_1m_win95]  [10M][d_10m_win95] |
| Path(finder)  |   104   |  2-63                   |            4.2        | [1K][d_1k_path] [10K][d_10k_path] [100K][d_100k_path] [1M][d_1m_path]  [10M][d_10m_path] |
| Andes         |   223   |  2-2                    |            2          | [1K][d_1k_andes] [10K][d_10k_andes] [100K][d_100k_andes] [1M][d_1m_andes]  [10M][d_10m_andes] |

## Random Queries
Queries used for evaluating the various counting strategies for [Child][rand_query_20], [Insur][rand_query_27], [Mild][rand_query_34], [Alarm][rand_query_37], [Barley][rand_query_46], [Hail][rand_query_56], [Hepar2][rand_query_70], [Win95][rand_query_74] and [Path][rand_query_104].


Following table contains the output file generated after running random query experiment. `results` in the following table directs to the output files of respective counting query engines which are zipped into one file.              
Format of the output files: Time spent in answering query | _Pa(X<sub>i</sub>)_ | _X<sub>i</sub>_                
Ex: `12.000000us| 5 21 |13 ` where Query(_X<sub>13</sub>_, _Pa(X<sub>13</sub>))_ took _12us_ and _Pa(X<sub>13</sub>)_ = {_X<sub>5</sub>_, _X<sub>21</sub>_}      


|       m    | Child                        |  Insur   | Mild  | Alarm | Barley    | Hail  | Hepar2 | Win95    | Path  |
| :--------: | :----:                       | :---:   | :---: | :---: | :----:    | :--:  | :---: | :--:      | :---: | 
|  1K        | [results][rand_res_1k_child] | [results][rand_res_1k_insur] | [results][rand_res_1k_mild] | [results][rand_res_1k_alarm] | [results][rand_res_1k_barley] | [results][rand_res_1k_hail] | [results][rand_res_1k_hepar2] | [results][rand_res_1k_win95] | [results][rand_res_1k_path] |
|  10K        | [results][rand_res_10k_child] | [results][rand_res_10k_insur] | [results][rand_res_10k_mild] | [results][rand_res_10k_alarm] | [results][rand_res_10k_barley] | [results][rand_res_10k_hail] | [results][rand_res_10k_hepar2] | [results][rand_res_10k_win95] | [results][rand_res_10k_path] |
|  100K        | [results][rand_res_100k_child] | [results][rand_res_100k_insur] | [results][rand_res_100k_mild] | [results][rand_res_100k_alarm] | [results][rand_res_100k_barley] | [results][rand_res_100k_hail] | [results][rand_res_100k_hepar2] | [results][rand_res_100k_win95] | [results][rand_res_100k_path] |
|  1M        | [results][rand_res_1m_child] | [results][rand_res_1m_insur] | [results][rand_res_1m_mild] | [results][rand_res_1m_alarm] | [results][rand_res_1m_barley] | [results][rand_res_1m_hail] | [results][rand_res_1m_hepar2] | [results][rand_res_1m_win95] | [results][rand_res_1m_path] |
|  10M        | [results][rand_res_10m_child] | [results][rand_res_10m_insur] | [results][rand_res_10m_mild] | [results][rand_res_10m_alarm] | [results][rand_res_10m_barley] | [results][rand_res_10m_hail] | [results][rand_res_10m_hepar2] | [results][rand_res_10m_win95] | [results][rand_res_10m_path] |

Following plots in the table show how the distribution of response time in microseconds, computed from the same sample of 1,000 queries for different number of instances _m_. Y-axis is in log<sub>10</sub>-scale.             
 
|m  |           Child           |           Insurance      |         Mildew         |           Alarm        |          Barley          |       Hailfinder      |           Hepar2          |       Win95pts         | Pathfinder |
| -- |     ------     |        -----     |        --------      |     -----------     |       --------          |     -------   |       ---------       |       ------------        |         ---------     | ----- |
|1K | ![alt text][r1k_child]    |   ![alt text][r1k_insur] | ![alt text][r1k_mild]  | ![alt text][r1k_alarm] | ![alt text][r1k_barley]  | ![alt text][r1k_hail] |![alt text][r1k_hepar2]    | ![alt text][r1k_win95] | ![alt text][r1k_path] |
|10K | ![alt text][r10k_child]    |   ![alt text][r10k_insur] | ![alt text][r10k_mild]  | ![alt text][r10k_alarm] | ![alt text][r10k_barley]  | ![alt text][r10k_hail] |![alt text][r10k_hepar2]    | ![alt text][r10k_win95] | ![alt text][r10k_path] | 
|100K | ![alt text][r100k_child]    |   ![alt text][r100k_insur] | ![alt text][r100k_mild]  | ![alt text][r100k_alarm] | ![alt text][r100k_barley]  | ![alt text][r100k_hail] |![alt text][r100k_hepar2]    | ![alt text][r100k_win95] | ![alt text][r100k_path] | 
|1M | ![alt text][r1m_child]    |   ![alt text][r1m_insur] | ![alt text][r1m_mild]  | ![alt text][r1m_alarm] | ![alt text][r1m_barley]  | ![alt text][r1m_hail] |![alt text][r1m_hepar2]    | ![alt text][r1m_win95] | ![alt text][r1m_path] | 
|10M | ![alt text][r10m_child]    |   ![alt text][r10m_insur] | ![alt text][r10m_mild]  | ![alt text][r10m_alarm] | ![alt text][r10m_barley]  | ![alt text][r10m_hail] |![alt text][r10m_hepar2]    | ![alt text][r10m_win95] | ![alt text][r10m_path] | 

Comparison of ADT, Hash, BMap, and Rad for different sizes of _Pa_ (x-axis) in the sample of 1,000 uniformly random queries. The plot shows the average query response time in microseconds. Y-axis is in log<sub>10</sub>-scale.           

| Child     | Alarm     | Hailfinder    | Other datasets |
| :----     | :----:    | :--------:    | -------------: |
|  ![alt text][l1k_child] | ![alt text][l1k_child] | ![alt text][l1k_child] | [Insur(1K)][l1K_insur] <br/> [Mild(1K)][l1K_mild] <br/> [Barley(1K)][l1K_barley] <br/> [Hepar2(1K)][l1K_hepar2] <br/> [Win95(1K)][l1K_win95] <br/> [Path(1K)][l1K_path] |
|  ![alt text][l10k_child] | ![alt text][l10k_child] | ![alt text][l10k_child] | [Insur(10K)][l10K_insur] <br/> [Mild(10K)][l10K_mild] <br/> [Barley(10K)][l10K_barley] <br/> [Hepar2(10K)][l10K_hepar2] <br/> [Win95(10K)][l10K_win95] <br/> [Path(10K)][l10K_path] |
|  ![alt text][l100k_child] | ![alt text][l100k_child] | ![alt text][l100k_child] | [Insur(100K)][l100K_insur] <br/> [Mild(100K)][l100K_mild] <br/> [Barley(100K)][l100K_barley] <br/> [Hepar2(100K)][l100K_hepar2] <br/> [Win95(100K)][l100K_win95] <br/> [Path(100K)][l100K_path] |
|  ![alt text][l1m_child] | ![alt text][l1m_child] | ![alt text][l1m_child] | [Insur(1M)][l1M_insur] <br/> [Mild(1M)][l1M_mild] <br/> [Barley(1M)][l1M_barley] <br/> [Hepar2(1M)][l1M_hepar2] <br/> [Win95(1M)][l1M_win95] <br/> [Path(1M)][l1M_path] |
|  ![alt text][l10m_child] | ![alt text][l10m_child] | ![alt text][l10m_child] | [Insur(10M)][l10M_insur] <br/> [Mild(10M)][l10M_mild] <br/> [Barley(10M)][l10M_barley] <br/> [Hepar2(10M)][l10M_hepar2] <br/> [Win95(10M)][l10M_win95] <br/> [Path(10M)][l10M_path] |


## Queries in Bayesian Networks Learning
In order to have the computations feasible, datasets like _Hepar2_, _Win95_, _Path_ and _Andes_ where sampled for 40 random variables without replacement to generate more datasets. The sampled datasets are summarized in the following table.

| Dataset               |    n    |  Range of r<sub>i</sub> | Average r<sub>i</sub> |       m   |
| :--------             | :-----: | :---------------------: | :-------------------: |   :-----: |
| Hepar2(1)             |   40    |  2-2                    |            2          |   [1K][bn_data_1k_hepar_1] [10K] [bn_data_10k_hepar_1] [100K] [bn_data_100k_hepar_1] |
| Hepar2(2)             |   40    |  2-4                    |            2.55       |   [1K][bn_data_1k_hepar_2] [10K] [bn_data_10k_hepar_2] [100K] [bn_data_100k_hepar_2] |
| Hepar2(3)             |   40    |  2-2                    |            2          |   [1K][bn_data_1k_hepar_3] [10K] [bn_data_10k_hepar_3] [100K] [bn_data_100k_hepar_3] |
| Win95(1)              |   40    |  2-2                    |            2          |   [1K][bn_data_1k_win95_1] [10K] [bn_data_10k_win95_1] [100K] [bn_data_100k_win95_1] |
| Win95(2)              |   40    |  2-2                    |            2          |   [1K][bn_data_1k_win95_2] [10K] [bn_data_10k_win95_2] [100K] [bn_data_100k_win95_2] |
| Win95(3)              |   40    |  2-2                    |            2          |   [1K][bn_data_1k_win95_3] [10K] [bn_data_10k_win95_3] [100K] [bn_data_100k_win95_3] |
| Path(1)               |   40    |  2-3                    |            2.2        |   [1K][bn_data_1k_path_1] [10K] [bn_data_10k_path_1] [100K] [bn_data_100k_path_1] |
| Path(2)               |   40    |  4-63                   |            6.65       |   [1K][bn_data_1k_path_2] [10K] [bn_data_10k_path_2] [100K] [bn_data_100k_path_2] |
| Path(3)               |   40    |  3-5                    |            4.33       |   [1K][bn_data_1k_path_3] [10K] [bn_data_10k_path_3] [100K] [bn_data_100k_path_3] |
| Andes(1)              |   40    |  2-2                    |            2.0        |   [1K][bn_data_1k_andes_1] [10K] [bn_data_10k_andes_1] [100K] [bn_data_100k_andes_1] |
| Andes(2)              |   40    |  2-2                    |            2.0        |   [1K][bn_data_1k_andes_2] [10K] [bn_data_10k_andes_2] [100K] [bn_data_100k_andes_2] |
| Andes(3)              |   40    |  2-2                    |            2.0        |   [1K][bn_data_1k_andes_3] [10K] [bn_data_10k_andes_3] [100K] [bn_data_100k_andes_3] |

Following plots describe the total execution time of the parent set assignment solver with ADT, Hash, BMap and Rad strategies, normalized with respect to the fastest method. The solver was executed up to the level where _|Pa|=6_.

![alt_text][mps_d1] 
![alt_text][mps_d2] 
![alt_text][mps_d3] 

[mps_d1]: img/mps/dataset1_mps.png
[mps_d2]: img/mps/dataset2_mps.png
[mps_d3]: img/mps/dataset3_mps.png

## Queries in Association Rule Mining
For association rule mining, the above described datasets where binarized to generate transactional datasets. 

Following plots show the total execution time of association rule mining with ADT, Hash, BMap and Rad strategies, normalized with respect to the fastest method. The solver was executed up to the level where _|Pa|=6_.

|   Plots   |   Dataset |
| :------   |   :-----: |
| ![alt_text][ass_plot_100k]    | [All files(100K)][d_100k_all_file_bin] <br/> [Child(100K)][d_100k_child_bin] <br/>  [Insur(100K)][d_100k_insur_bin] <br/>  [Mild(100K)][d_100k_mild_bin] <br/>  [Alarm(100K)][d_100k_alarm_bin] <br/>  [Barley(100K)][d_100k_barley_bin] <br/>  [Hail(100K)][d_100k_hail_bin] <br/>  [Win95(100K)][d_100k_win95_bin] <br/>  [Path(100K)][d_100k_path_bin] |
| ![alt_text][ass_plot_1m]    | [All files(1M)][d_1m_all_file_bin] <br/> [Child(1M)][d_1m_child_bin] <br/>  [Insur(1M)][d_1m_insur_bin] <br/>  [Mild(1M)][d_1m_mild_bin] <br/>  [Alarm(1M)][d_1m_alarm_bin] <br/>  [Barley(1M)][d_1m_barley_bin] <br/>  [Hail(1M)][d_1m_hail_bin] <br/>  [Win95(1M)][d_1m_win95_bin] <br/>  [Path(1M)][d_1m_path_bin] |
| ![alt_text][ass_plot_10m] | [All files(10M)][d_10m_all_file_bin] <br/> [Child(10M)][d_10m_child_bin] <br/>  [Insur(10M)][d_10m_insur_bin] <br/>  [Mild(10M)][d_10m_mild_bin] <br/>  [Alarm(10M)][d_10m_alarm_bin] <br/>  [Barley(10M)][d_10m_barley_bin] <br/>  [Hail(10M)][d_10m_hail_bin] <br/>  [Win95(10M)][d_10m_win95_bin] <br/>  [Path(10M)][d_10m_path_bin] |



[ass_plot_100k]: img/ass/full_rule_mining_100k.png
[ass_plot_1m]: img/ass/full_rule_mining_1m.png
[ass_plot_10m]: img/ass/full_rule_mining_10m.png

[d_100k_child_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/child-binarized.zip
[d_100k_insur_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/insurance-binarized.zip
[d_100k_mild_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/mildew-binarized.zip
[d_100k_alarm_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/alarm-binarized.zip
[d_100k_barley_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/barley-binarized.zip
[d_100k_hail_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/hailfinder-binarized.zip
[d_100k_win95_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/win95pts-binarized.zip
[d_100k_path_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/pathfinder-binarized.zip
[d_100k_all_file_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/all_bin_100k.zip


[d_1m_child_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/child-binarized.zip
[d_1m_insur_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/insurance-binarized.zip
[d_1m_mild_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/mildew-binarized.zip
[d_1m_alarm_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/alarm-binarized.zip
[d_1m_barley_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/barley-binarized.zip
[d_1m_hail_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/hailfinder-binarized.zip
[d_1m_win95_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/win95pts-binarized.zip
[d_1m_path_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/pathfinder-binarized.zip
[d_1m_all_file_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/all_bin_1m.zip

[d_10m_child_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/child-binarized.zip
[d_10m_insur_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/insurance-binarized.zip
[d_10m_mild_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/mildew-binarized.zip
[d_10m_alarm_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/alarm-binarized.zip
[d_10m_barley_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/barley-binarized.zip
[d_10m_hail_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/hailfinder-binarized.zip
[d_10m_win95_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/win95pts-binarized.zip
[d_10m_path_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/pathfinder-binarized.zip
[d_10m_all_file_bin]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/all_bin_10m.zip


[d_1k_child]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/alarm.zip
[d_1k_insur]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/insurance.zip
[d_1k_mild]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/mildew.zip
[d_1k_alarm]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/alarm.zip
[d_1k_barley]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/barley.zip
[d_1k_hail]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/hailfinder.zip
[d_1k_hepar2]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/hepar2.zip
[d_1k_win95]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/win95pts.zip
[d_1k_path]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/pathfinder.zip
[d_1k_andes]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1K/andes.zip

[d_10k_child]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/alarm.zip
[d_10k_insur]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/insurance.zip
[d_10k_mild]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/mildew.zip
[d_10k_alarm]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/alarm.zip
[d_10k_barley]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/barley.zip
[d_10k_hail]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/hailfinder.zip
[d_10k_hepar2]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/hepar2.zip
[d_10k_win95]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/win95pts.zip
[d_10k_path]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/pathfinder.zip
[d_10k_andes]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10K/andes.zip

[d_100k_child]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/alarm.zip
[d_100k_insur]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/insurance.zip
[d_100k_mild]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/mildew.zip
[d_100k_alarm]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/alarm.zip
[d_100k_barley]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/barley.zip
[d_100k_hail]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/hailfinder.zip
[d_100k_hepar2]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/hepar2.zip
[d_100k_win95]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/win95pts.zip
[d_100k_path]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/pathfinder.zip
[d_100k_andes]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/100K/andes.zip

[d_1m_child]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/alarm.zip
[d_1m_insur]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/insurance.zip
[d_1m_mild]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/mildew.zip
[d_1m_alarm]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/alarm.zip
[d_1m_barley]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/barley.zip
[d_1m_hail]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/hailfinder.zip
[d_1m_hepar2]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/hepar2.zip
[d_1m_win95]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/win95pts.zip
[d_1m_path]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/pathfinder.zip
[d_1m_andes]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/1M/andes.zip

[d_10m_child]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/alarm.zip
[d_10m_insur]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/insurance.zip
[d_10m_mild]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/mildew.zip
[d_10m_alarm]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/alarm.zip
[d_10m_barley]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/barley.zip
[d_10m_hail]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/hailfinder.zip
[d_10m_hepar2]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/hepar2.zip
[d_10m_win95]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/win95pts.zip
[d_10m_path]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/pathfinder.zip
[d_10m_andes]: https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks/blob/master/data/10M/andes.zip


[r1k_child]: img/rand/1K/child.png
[r1k_insur]: img/rand/1K/insurance.png
[r1k_mild]: img/rand/1K/mildew.png
[r1k_alarm]: img/rand/1K/alarm.png
[r1k_barley]: img/rand/1K/barley.png
[r1k_hail]: img/rand/1K/hailfinder.png
[r1k_hepar2]: img/rand/1K/hepar2.png
[r1k_win95]: img/rand/1K/win95pts.png
[r1k_path]: img/rand/1K/pathfinder.png

[r10k_child]: img/rand/10K/child.png
[r10k_insur]: img/rand/10K/insurance.png
[r10k_mild]: img/rand/10K/mildew.png
[r10k_alarm]: img/rand/10K/alarm.png
[r10k_barley]: img/rand/10K/barley.png
[r10k_hail]: img/rand/10K/hailfinder.png
[r10k_hepar2]: img/rand/10K/hepar2.png
[r10k_win95]: img/rand/10K/win95pts.png
[r10k_path]: img/rand/10K/pathfinder.png

[r100k_child]: img/rand/100K/child.png
[r100k_insur]: img/rand/100K/insurance.png
[r100k_mild]: img/rand/100K/mildew.png
[r100k_alarm]: img/rand/100K/alarm.png
[r100k_barley]: img/rand/100K/barley.png
[r100k_hail]: img/rand/100K/hailfinder.png
[r100k_hepar2]: img/rand/100K/hepar2.png
[r100k_win95]: img/rand/100K/win95pts.png
[r100k_path]: img/rand/100K/pathfinder.png

[r1m_child]: img/rand/1M/child.png
[r1m_insur]: img/rand/1M/insurance.png
[r1m_mild]: img/rand/1M/mildew.png
[r1m_alarm]: img/rand/1M/alarm.png
[r1m_barley]: img/rand/1M/barley.png
[r1m_hail]: img/rand/1M/hailfinder.png
[r1m_hepar2]: img/rand/1M/hepar2.png
[r1m_win95]: img/rand/1M/win95pts.png
[r1m_path]: img/rand/1M/pathfinder.png

[r10m_child]: img/rand/10M/child.png
[r10m_insur]: img/rand/10M/insurance.png
[r10m_mild]: img/rand/10M/mildew.png
[r10m_alarm]: img/rand/10M/alarm.png
[r10m_barley]: img/rand/10M/barley.png
[r10m_hail]: img/rand/10M/hailfinder.png
[r10m_hepar2]: img/rand/10M/hepar2.png
[r10m_win95]: img/rand/10M/win95pts.png
[r10m_path]: img/rand/10M/pathfinder.png

[l1k_child]: img/rand/layer_wise/layer_1k_child.png
[l10k_child]: img/rand/layer_wise/layer_10k_child.png
[l100k_child]: img/rand/layer_wise/layer_100k_child.png
[l1m_child]: img/rand/layer_wise/layer_1m_child.png
[l10m_child]: img/rand/layer_wise/layer_10m_child.png

[l1k_insur]: img/rand/layer_wise/layer_1k_insurance.png
[l10k_insur]: img/rand/layer_wise/layer_10k_insurance.png
[l100k_insur]: img/rand/layer_wise/layer_100k_insurance.png
[l1m_insur]: img/rand/layer_wise/layer_1m_insurance.png
[l10m_insur]: img/rand/layer_wise/layer_10m_insurance.png

[l1k_mild]: img/rand/layer_wise/layer_1k_mildew.png
[l10k_mild]: img/rand/layer_wise/layer_10k_mildew.png
[l100k_mild]: img/rand/layer_wise/layer_100k_mildew.png
[l1m_mild]: img/rand/layer_wise/layer_1m_mildew.png
[l10m_mild]: img/rand/layer_wise/layer_10m_mildew.png

[l1k_alarm]: img/rand/layer_wise/layer_1k_alarm.png
[l10k_alarm]: img/rand/layer_wise/layer_10k_alarm.png
[l100k_alarm]: img/rand/layer_wise/layer_100k_alarm.png
[l1m_alarm]: img/rand/layer_wise/layer_1m_alarm.png
[l10m_alarm]: img/rand/layer_wise/layer_10m_alarm.png

[l1k_barley]: img/rand/layer_wise/layer_1k_barley.png
[l10k_barley]: img/rand/layer_wise/layer_10k_barley.png
[l100k_barley]: img/rand/layer_wise/layer_100k_barley.png
[l1m_barley]: img/rand/layer_wise/layer_1m_barley.png
[l10m_barley]: img/rand/layer_wise/layer_10m_barley.png

[l1k_hail]: img/rand/layer_wise/layer_1k_hail.png
[l10k_hail]: img/rand/layer_wise/layer_10k_hail.png
[l100k_hail]: img/rand/layer_wise/layer_100k_hail.png
[l1m_hail]: img/rand/layer_wise/layer_1m_hail.png
[l10m_hail]: img/rand/layer_wise/layer_10m_hail.png

[l1k_hepar2]: img/rand/layer_wise/layer_1k_hepar2.png
[l10k_hepar2]: img/rand/layer_wise/layer_10k_hepar2.png
[l100k_hepar2]: img/rand/layer_wise/layer_100k_hepar2.png
[l1m_hepar2]: img/rand/layer_wise/layer_1m_hepar2.png
[l10m_hepar2]: img/rand/layer_wise/layer_10m_hepar2.png

[l1k_win95]: img/rand/layer_wise/layer_1k_win95pts.png
[l10k_win95]: img/rand/layer_wise/layer_10k_win95pts.png
[l100k_win95]: img/rand/layer_wise/layer_100k_win95pts.png
[l1m_win95]: img/rand/layer_wise/layer_1m_win95pts.png
[l10m_win95]: img/rand/layer_wise/layer_10m_win95pts.png

[l1k_path]: img/rand/layer_wise/layer_1k_pathfinder.png
[l10k_path]: img/rand/layer_wise/layer_10k_pathfinder.png
[l100k_path]: img/rand/layer_wise/layer_100k_pathfinder.png
[l1m_path]: img/rand/layer_wise/layer_1m_pathfinder.png
[l10m_path]: img/rand/layer_wise/layer_10m_pathfinder.png

[rand_query_20]: data/queries/20_1K_default_seed.query
[rand_query_27]: data/queries/27_1K_default_seed.query
[rand_query_34]: data/queries/34_1K_default_seed.query
[rand_query_37]: data/queries/37_1K_default_seed.query
[rand_query_46]: data/queries/46_1K_default_seed.query
[rand_query_56]: data/queries/56_1K_default_seed.query
[rand_query_70]: data/queries/70_1K_default_seed.query
[rand_query_74]: data/queries/74_1K_default_seed.query
[rand_query_104]: data/queries/104_1K_default_seed.query

[rand_res_1k_child]: random_query_results/1K/adt_hash_bmap_rad_child.zip
[rand_res_1k_insur]: random_query_results/1K/adt_hash_bmap_rad_insurance.zip
[rand_res_1k_mild]: random_query_results/1K/adt_hash_bmap_rad_mildew.zip
[rand_res_1k_alarm]: random_query_results/1K/adt_hash_bmap_rad_alarm.zip
[rand_res_1k_barley]: random_query_results/1K/adt_hash_bmap_rad_barley.zip
[rand_res_1k_hail]: random_query_results/1K/adt_hash_bmap_rad_hailfinder.zip
[rand_res_1k_hepar2]: random_query_results/1K/adt_hash_bmap_rad_hepar2.zip
[rand_res_1k_win95]: random_query_results/1K/adt_hash_bmap_rad_win95pts.zip
[rand_res_1k_path]: random_query_results/1K/adt_hash_bmap_rad_pathfinder.zip

[rand_res_10k_child]: random_query_results/10K/adt_hash_bmap_rad_child.zip
[rand_res_10k_insur]: random_query_results/10K/adt_hash_bmap_rad_insurance.zip
[rand_res_10k_mild]: random_query_results/10K/adt_hash_bmap_rad_mildew.zip
[rand_res_10k_alarm]: random_query_results/10K/adt_hash_bmap_rad_alarm.zip
[rand_res_10k_barley]: random_query_results/10K/adt_hash_bmap_rad_barley.zip
[rand_res_10k_hail]: random_query_results/10K/adt_hash_bmap_rad_hailfinder.zip
[rand_res_10k_hepar2]: random_query_results/10K/adt_hash_bmap_rad_hepar2.zip
[rand_res_10k_win95]: random_query_results/10K/adt_hash_bmap_rad_win95pts.zip
[rand_res_10k_path]: random_query_results/10K/adt_hash_bmap_rad_pathfinder.zip

[rand_res_100k_child]: random_query_results/100K/adt_hash_bmap_rad_child.zip
[rand_res_100k_insur]: random_query_results/100K/adt_hash_bmap_rad_insurance.zip
[rand_res_100k_mild]: random_query_results/100K/adt_hash_bmap_rad_mildew.zip
[rand_res_100k_alarm]: random_query_results/100K/adt_hash_bmap_rad_alarm.zip
[rand_res_100k_barley]: random_query_results/100K/adt_hash_bmap_rad_barley.zip
[rand_res_100k_hail]: random_query_results/100K/adt_hash_bmap_rad_hailfinder.zip
[rand_res_100k_hepar2]: random_query_results/100K/adt_hash_bmap_rad_hepar2.zip
[rand_res_100k_win95]: random_query_results/100K/adt_hash_bmap_rad_win95pts.zip
[rand_res_100k_path]: random_query_results/100K/adt_hash_bmap_rad_pathfinder.zip
 
[rand_res_1m_child]: random_query_results/1M/adt_hash_bmap_rad_child.zip
[rand_res_1m_insur]: random_query_results/1M/adt_hash_bmap_rad_insurance.zip
[rand_res_1m_mild]: random_query_results/1M/adt_hash_bmap_rad_mildew.zip
[rand_res_1m_alarm]: random_query_results/1M/adt_hash_bmap_rad_alarm.zip
[rand_res_1m_barley]: random_query_results/1M/adt_hash_bmap_rad_barley.zip
[rand_res_1m_hail]: random_query_results/1M/adt_hash_bmap_rad_hailfinder.zip
[rand_res_1m_hepar2]: random_query_results/1M/adt_hash_bmap_rad_hepar2.zip
[rand_res_1m_win95]: random_query_results/1M/adt_hash_bmap_rad_win95pts.zip
[rand_res_1m_path]: random_query_results/1M/adt_hash_bmap_rad_pathfinder.zip

[rand_res_10m_child]: random_query_results/10M/adt_hash_bmap_rad_child.zip
[rand_res_10m_insur]: random_query_results/10M/adt_hash_bmap_rad_insurance.zip
[rand_res_10m_mild]: random_query_results/10M/adt_hash_bmap_rad_mildew.zip
[rand_res_10m_alarm]: random_query_results/10M/adt_hash_bmap_rad_alarm.zip
[rand_res_10m_barley]: random_query_results/10M/adt_hash_bmap_rad_barley.zip
[rand_res_10m_hail]: random_query_results/10M/adt_hash_bmap_rad_hailfinder.zip
[rand_res_10m_hepar2]: random_query_results/10M/adt_hash_bmap_rad_hepar2.zip
[rand_res_10m_win95]: random_query_results/10M/adt_hash_bmap_rad_win95pts.zip
[rand_res_10m_path]: random_query_results/10M/adt_hash_bmap_rad_pathfinder.zip

[bn_data_1k_hepar_1]: data/1K/hepar2_rand_1.zip
[bn_data_1k_hepar_2]: data/1K/hepar2_rand_2.zip
[bn_data_1k_hepar_3]: data/1K/hepar2_rand_3.zip
[bn_data_1k_path_1]: data/1K/pathfinder_rand_1.zip
[bn_data_1k_path_2]: data/1K/pathfinder_rand_2.zip
[bn_data_1k_path_3]: data/1K/pathfinder_rand_3.zip
[bn_data_1k_win95_1]: data/1K/win95pts_rand_1.zip
[bn_data_1k_win95_2]: data/1K/win95pts_rand_2.zip
[bn_data_1k_win95_3]: data/1K/win95pts_rand_3.zip
[bn_data_1k_andes_1]: data/1K/andes_rand_1.zip
[bn_data_1k_andes_2]: data/1K/andes_rand_2.zip
[bn_data_1k_andes_3]: data/1K/andes_rand_3.zip

[bn_data_10k_hepar_1]: data/10K/hepar2_rand_1.zip
[bn_data_10k_hepar_2]: data/10K/hepar2_rand_2.zip
[bn_data_10k_hepar_3]: data/10K/hepar2_rand_3.zip
[bn_data_10k_path_1]: data/10K/pathfinder_rand_1.zip
[bn_data_10k_path_2]: data/10K/pathfinder_rand_2.zip
[bn_data_10k_path_3]: data/10K/pathfinder_rand_3.zip
[bn_data_10k_win95_1]: data/10K/win95pts_rand_1.zip
[bn_data_10k_win95_2]: data/10K/win95pts_rand_2.zip
[bn_data_10k_win95_3]: data/10K/win95pts_rand_3.zip
[bn_data_10k_andes_1]: data/10K/andes_rand_1.zip
[bn_data_10k_andes_2]: data/10K/andes_rand_2.zip
[bn_data_10k_andes_3]: data/10K/andes_rand_3.zip

[bn_data_100k_hepar_1]: data/100K/hepar2_rand_1.zip
[bn_data_100k_hepar_2]: data/100K/hepar2_rand_2.zip
[bn_data_100k_hepar_3]: data/100K/hepar2_rand_3.zip
[bn_data_100k_path_1]: data/100K/pathfinder_rand_1.zip
[bn_data_100k_path_2]: data/100K/pathfinder_rand_2.zip
[bn_data_100k_path_3]: data/100K/pathfinder_rand_3.zip
[bn_data_100k_win95_1]: data/100K/win95pts_rand_1.zip
[bn_data_100k_win95_2]: data/100K/win95pts_rand_2.zip
[bn_data_100k_win95_3]: data/100K/win95pts_rand_3.zip
[bn_data_100k_andes_1]: data/100K/andes_rand_1.zip
[bn_data_100k_andes_2]: data/100K/andes_rand_2.zip
[bn_data_100k_andes_3]: data/100K/andes_rand_3.zip
